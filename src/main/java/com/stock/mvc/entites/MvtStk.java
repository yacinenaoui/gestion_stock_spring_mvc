package com.stock.mvc.entites;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class MvtStk implements Serializable {
	public static final int ENTREE=1;
	public static final int SORTIE=2; 
	
	@Id
	@GeneratedValue
	private Long idMvmStk;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateMvm;
	private BigDecimal quantite;
	private int typeMvm;
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article;
	public Long getIdMvmStk() {
		return idMvmStk;
	}
	public void setIdMvmStk(Long idMvmStk) {
		this.idMvmStk = idMvmStk;
	}
	public Date getDateMvm() {
		return dateMvm;
	}
	public void setDateMvm(Date dateMvm) {
		this.dateMvm = dateMvm;
	}
	public BigDecimal getQuantite() {
		return quantite;
	}
	public void setQuantite(BigDecimal quantite) {
		this.quantite = quantite;
	}
	public int getTypeMvm() {
		return typeMvm;
	}
	public void setTypeMvm(int typeMvm) {
		this.typeMvm = typeMvm;
	}
	public Article getArticle() {
		return article;
	}
	public void setArticle(Article article) {
		this.article = article;
	}
}
