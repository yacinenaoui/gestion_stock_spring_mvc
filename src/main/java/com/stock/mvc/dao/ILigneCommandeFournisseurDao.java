package com.stock.mvc.dao;

import com.stock.mvc.entites.CommandeFournisseur;
import com.stock.mvc.entites.LigneCommandeFournisseur;

public interface ILigneCommandeFournisseurDao extends IGenericDao<LigneCommandeFournisseur> {

}
