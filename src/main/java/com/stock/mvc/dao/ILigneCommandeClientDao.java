package com.stock.mvc.dao;

import com.stock.mvc.entites.CommandeClient;
import com.stock.mvc.entites.LigneCommandeClient;

public interface ILigneCommandeClientDao extends IGenericDao<LigneCommandeClient> {

}
